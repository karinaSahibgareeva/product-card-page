$(document).ready(function () {
    $(".product-page__slider").owlCarousel({
        responsive: {
            0: {
                items: 1,
                loop: false,
                nav: false,
                dots: true,
            }
        }
    });
    $(".rec-products__slider").owlCarousel({
        margin: 40,
        responsive: {
            0: {
                items: 1,
                loop: false,
                nav: false,
                dots: true
            },
            760: {
                items: 2
            },
            1024: {
                items: 2.5,
            },
            1280: {
                items: 3,
                loop: false,
                nav: false,
                dots: true
            }
        }
    });
    Fancybox.bind("[data-fancybox=gallery]", {
        placeFocusBack: false,
        autoFocus: false,
        trapFocus: false,
    });
})
//  tabs
let tabItem = document.querySelectorAll('.info-block__tab');
let tabTxtBlock = document.querySelectorAll('.info-block__tab-cont');

tabItem.forEach(function (item, index) {
    item.addEventListener('click', () => {
        selectTab(index)
    })
})
function selectTab(ind) {
    tabItem.forEach(item => {
        item.classList.remove('i-active-tab');
    })
    tabItem[ind].classList.add('i-active-tab');
    tabTxtBlock.forEach((item, index) => {
        (index === ind) ? item.classList.add('active-tab-cont') : item.classList.remove('active-tab-cont');
    })
}
// order popup
$('.product-page__order-btn').click(function (e) {
    e.preventDefault()
    $('.order-popup').fadeIn();
    $('.overlay').fadeIn();
    $('body').css('overflow-y', 'hidden');
})
$('.modal-close').click(function () {
    $('.overlay').fadeOut();
    $('.order-popup').fadeOut();
    $('body').css('overflow-y', 'visible');
})
$('.overlay').click(function () {
    $('.order-popup').fadeOut();
    $('.overlay').fadeOut();
    $('body').css('overflow-y', 'visible');
})